��    .      �              �  y   �     w     �     �     �  #   �     �     �     �     �     �  �       �     �     �     �          	                )     .     @     D     I  l   V  !   �     �     �     �               1  -   E  �   s     �                3     D     V  �   j     �     �     		  �  	  �   �
     N     a     w     �  '   �     �     �     �     �     �  �    
   �     �             	        '     7     C     J     W     c     g     o  l   ~  &   �               (     5     U     s  $   �  �   �     f     l     �     �     �     �  �   �  
   �  
   �     �   ' is opened. That means all setting will be stored into this profile.
If you want to apply them, go to Profiles -> apply. About... Adjust device name Apply Apply Profile Apply one of the following profiles Backward Backward Button Buttons Change Device Name Close programm Currently, the Live Profile is opened. That means, that all changes will be applied directly into the driver settings.
With the help of the Profile menu you may create several profiles with different settings and also for several devices if needed.
When another Profile than the Live Profile is opened, all the settings you make will be stored in the profile and not applied directly.
Only by applying thoose profiles the settings will be applied Currently, the Profile ' Delete Delete Profile Down Forward Forward Button General Gestures Left Left Side Gesture New Open Open Profile Please check if this is the correct device name.
If it is, just click OK, if not, please adjust and click OK Please enter the new Device Name: Profiles Right Right Side Gesture Save to opened profile Select Action here Select Profile here Select one of the profiles which is to delete Select the Profile to open here.
Setting the Live Profile means, that all settings will be applied directly
and not safed to a profile. Set  Set backward button click Set forward button click Set left Gesture Set right Gesture Set upwards Gesture This Action is allready given.
Unfortunataly double entry actions would cause errors in this Programm version.
So the process was prevented. Up Upward Gesture Warning Project-Id-Version: django
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2022-01-25 16:44+0100
PO-Revision-Date: 2022-01-25 16:44+0100
Last-Translator: S60W79 <ernetnakisuml@gmail.com>
Language-Team: German
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 'ist geöffnet. Das heißt, alle Einstellungen werden in diesem Profil gespeichert.
Die Änderrungen werden erst Wirksam, wenn Sie das unter Profile->Anwenden einstellen. Über das Programm Gerätenamen anpassen Anwenden Profil anwenden Eines der folgenden Profile anwenden... Rückwärts Rückwärts Knopf Knöpfe Gerätenamen ändern Programm schließen Aktuell ist das Live Profil geöffnet. Das heist, alle Änderrungen werden direkt wirksam.
Mit Hilfe des Profile Menüs können sie verschiedene Profile mit unterschiedlichen einstellungen f ggf. auch für verschiedene Geräte einstellen.
Wenn ein anderes Profil, als das Live Profil geöffnet ist, werden alle vorgenommenen Einstellungen gespeichert, aber nicht sofort wirksam.
Erst wenn Sie das betreffende Profil Anwenden (Unter Profile -> Anwenden) werden die Änderrungen gültig Das Profil Löschen Profil löschen Runterziehen Forwärts Forwärts Knopf Allgemeines Gesten Links-ziehen Links-Geste Neu Öffnen Profil öffnen Bitte prüfen Sie den Gerätenamen.
Wenn er korrekt ist, clicken Sie OK, wenn nicht passen Sie ihn bitte an. Bitte den neuen Gerätenamen eingeben: Profile Rechts-ziehen Rechts-Geste In geöffnetes Profil speichern Bitte Aktion hier auswählen: Profil hier auswählen Das zu löschende Profil auswählen: Wählen sie hier das zu öffnende Profil aus.
Die Einstellung 'Live Profil' bedeutet, das alle Einstellungen direkt wirksam,
jedoch nicht in einem separaten Profil gespeichert werden Setze Reaktion auf Rückwärts click Reaktion auf Forwärts-Click 'links-ziehen' Geste setzen 'rechts-ziehen' Geste setzen 'Hochziehen' Geste setzen Diese Reaktion wurde bereits für ein anderes Ereignis vergeben
Leider ist es aktuell nicht möglich Aktionen mehrfach zu setzen, da das Fehler in dieser Programm
Version verursachen würde.
Die Aktion wurde deshalb nicht gesetzt Hochziehen Hoch-Geste Warnung 