#!/usr/bin/python3
from locale import getdefaultlocale 
import gettext
import tkinter as tk
import os
import time
_ = gettext.gettext
wanted = getdefaultlocale()[0]
if wanted.find('de') != -1:
    de=gettext.translation('base', localedir='/usr/share/logiops-configurator/locale', languages=['de'])
    de.install()
    _ = de.gettext
pwd = ""
#detect environment and combine useful key combinations
environment = os.environ.get('DESKTOP_SESSION')
#To find out standart shortcuts
detected = False
general = {"Copy" : '"KEY_LEFTCTRL", "KEY_C"', "Cut" : '"KEY_LEFTCTRL", "KEY_X"', "Paste" : '"KEY_LEFTCTRL", "KEY_V"', 'Next Tab' : '"KEY_LEFTALT", "KEY_PAGEUP"', 'Previous Tab': '"KEY_LEFTCTRL", "KEY_PAGEDOWN"', "Back/Undo" : '"KEY_BACK"', "Forward/Redo":'"KEY_FORWARD"', "Shift window up":'"KEY_LEFTMETA", "KEY_UP"', "Shift window down":'"KEY_LEFTMETA", "KEY_DOWN"', "Shift window left":'"KEY_LEFTMETA", "KEY_LEFT"', "Shift window right":'"KEY_LEFTMETA", "KEY_RIGHT"'}
if environment == "plasma":
    detected = True
    enkeys = {"KRunner (KDE)" : '"KEY_LEFTALT", "KEY_SPACE"', "System Activity" : '"KEY_LEFTCTRL", "KEY_ESC"', "System Activity (KDE)" : '"KEY_LEFTCTRL", "KEY_ESC"'}
if detected:
    keys = {**general, **enkeys}

    
class profile(object):
    #Profile class = all posible settings of a file
    def findKeyStart(self, modus, useDraft):
        #finds the start of the Key Combination Identifier in the cfg File's text
        if useDraft:
            start = self.draft.find(modus)
            return self.draft.find("[", start)+1
        else:
            start = self.now.find(modus)
            return self.now.find("[", start)+1
    def keyfind(self, modus, useDraft):
        #Reads out the Key Combination identifier in the cfg File's text
        start = self.findKeyStart(modus, useDraft)
        if useDraft:
           posb = self.draft.find("]", start)
           return self.draft[start:posb]
        else:
            posb = self.now.find("]", start)
            return self.now[start:posb]
    def fromFile(self):
        if self.fname == "logid":
            domain = '/etc/'
        else:
            domain = '/usr/share/logiops-configurator/'
        cfgfile = open(domain+self.fname+".cfg", "a")
        #this will create the file if it does'nt exist yet
        cfgfile.close()
        cfgfile = open(domain+self.fname+".cfg")
        self.now = cfgfile.read()  
        self.draft = self.now
    def read(self, useDraft):
        self.bck = self.keyfind("0x56", useDraft)
        self.fwd = self.keyfind("0x53", useDraft)
        self.up = self.keyfind("Up", useDraft)
        self.down = self.keyfind("Down", useDraft)
        self.left = self.keyfind("Left", useDraft)
        self.right = self.keyfind("Right", useDraft)
    def overwrite(self, modus, action, useDraft):
        #writes pre-saved keys to self.now or self.draft .
        old = self.keyfind(modus, useDraft)
        if useDraft:
            if self.draft.find(action) == -1:
                self.draft = self.draft.replace(old, action)
            else:
                error = report(_("This Action is allready given.\nUnfortunataly double entry actions would cause errors in this Programm version.\nSo the process was prevented."), _("Warning"))
        else:
            if self.now.find(action) == -1:
                self.now = self.now.replace(old, action)
            else:
                error = report(_("This Action is allready given.\nUnfortunataly double entry actions would cause errors in this Programm version.\nSo the process was prevented."), _("Warning"))
    def fName(self, useDraft):
        #find Device Name as given in live file or draft
        if useDraft:
           start = self.draft.find('"')
           posb = self.draft.find('"', start)
           return self.draft[start:posb]
        else:
            start = self.draft.find('"')+1
            posb = self.now.find('"', start)
            return self.now[start:posb]
    def chName(self, new, useDraft):
        #set new Device name
        if useDraft:
           old = self.fName(useDraft)
           self.draft = self.draft.replace(old, new)
        else:
            old = self.fName(useDraft)
            self.now = self.now.replace(old, new)
    def safe(self, useDraft):
        if self.fname == "logid":
            cfgfile = open("/etc/"+self.fname+".cfg", "w")
        else:
            cfgfile = open("/usr/share/logiops-configurator/"+self.fname+".cfg", "w")
        if useDraft:
            cfgfile.write(self.draft)
        else:
            cfgfile.write(self.now)
        cfgfile.close()
        self.read(useDraft)

    def __init__(self, fname):
        self.fname = fname
        self.device = ""
        self.bck = ""
        self.fwd = ""
        self.up = '"KEY_LEFTMETA", "KEY_UP"'
        self.down = '"KEY_LEFTMETA", "KEY_DOWN"'
        self.left = '"KEY_LEFTMETA", "KEY_LEFT"'
        self.right = '"KEY_LEFTMETA", "KEY_RIGHT"'
        self.now = ""
        self.draft = ""

class tinyWin(object):
    def __init__(self, description, title):
        self.root = tk.Tk()
        self.root.geometry('450x150')
        self.root.title(title)
        self.des = tk.Label(self.root, text=description)
        self.des.grid(row=0, column=0)
class chooseWin(tinyWin):
    #defines a window, where a title, description, an option field and apply button is given
    def __init__(self, description, title, options):
        super().__init__(description, title)
        self.plain  = tk.StringVar(self.root)
        self.choose = tk.OptionMenu(self.root, self.plain, *tuple(options))
        self.applybu =tk.Button(self.root, text="OK", bg='green', command=self.execute)
        self.choose.config(width=30)
        self.choose.grid(row=1, column=0)
        self.applybu.grid(row=2, column=0)
        
class simplechoose(chooseWin):
    #specifies chooseWin, so it also has a 'apply to Profile' button.
    def execute(self):
        if self.plain.get.find(' ') != -1:
            #ensure that it is not the preset, which is NOT an option.
            live.overwrite(self.modus, keys[self.plain.get()], 0)
            live.safe(0)
            self.root.destroy()
    def __init__(self, device, description, modus, profile):
        global keys
        options = list(keys.keys())
        super().__init__(description, _('Set ')+device, options)
        old = live.keyfind(modus, False)
        if old in keys.values():
            #if there is an trivial name for this shortcut find it nad show it.
            for alias, tech in keys.items():
                print(alias)
                print(tech)
                if old == tech:
                    old = alias
        #the old shotcut is previous set in the OptionMenu
        self.plain.set(old)
        self.profilebu =tk.Button(self.root, text=_("Save to opened profile"), command=self.execute)
        self.modus = modus
        if profile:
            self.profilebu.grif(row=2, column=1)
        self.root.mainloop()
class profileFile(chooseWin):
    #specifies chooseWin, so the user may choose a profile from /usr/share/logiops-configurator
    def __init__(self, title, description):
        options = os.listdir("/usr/share/logiops-configurator")
        options.append("Live Profile")
        print(options)
        super().__init__(description, title, options)
        self.plain.set(_("Select Profile here"))
class openFile(profileFile):
    #specifies profileFile, so that the user chooses a Profile to open
    def execute(self):
        if ' ' in self.plain.get() and self.plain.get() != "Live Profile":
            #this is the preset or an file with a spacemark. cancel!
            print("Error, Filename with space, or preset option")
            return
        
        global advice
        if self.plain.get() == "Live Profile":
            live.fname = "logid"
            advice.config(text=_("Currently, the Live Profile is opened. That means, that all changes will be applied directly into the driver settings.\nWith the help of the Profile menu you may create several profiles with different settings and also for several devices if needed.\nWhen another Profile than the Live Profile is opened, all the settings you make will be stored in the profile and not applied directly.\nOnly by applying thoose profiles the settings will be applied"))
        else:
            live.fname = self.plain.get().replace('.cfg', '')
            advice.config(text = _("Currently, the Profile '")+live.fname+_("' is opened. That means all setting will be stored into this profile.\nIf you want to apply them, go to Profiles -> apply."))
        live.fromFile()
        live.read(0)
        self.root.destroy()
    def __init__(self):
        super().__init__(_("Open Profile"), _("Select the Profile to open here.\nSetting the Live Profile means, that all settings will be applied directly\nand not safed to a profile."))
        self.root.mainloop()
class applyFile(profileFile):
    def execute(self):
        if self.plain.get() != "Live Profile":
            oldname = live.fname
            live.fname = self.plain.get()
            live.read(False)
            live.fname = "logid"
            live.safe(False)
            live.fname=oldname
        self.root.destroy()
    def __init__(self):
        super().__init__(_("Apply Profile"), _("Apply one of the following profiles"))
        self.root.mainloop()
        
class delFile(profileFile):
    def execute(self):
        if self.plain.get() != "Live Profile":
            os.popen("rm /usr/share/logiops-configurator/"+self.plain.get())
            if self.plain.get().replace('.cfg', '') == live.fname:
                #profile to delete is currently open -> open live profile after deleting
                live.fname = "logid"
                live.fromFile()
                live.read(False)
                global advice
                advice.config(text=_("Currently, the Live Profile is opened. That means, that all changes will be applied directly into the driver settings.\nWith the help of the Profile menu you may create several profiles with different settings and also for several devices if needed.\nWhen another Profile than the Live Profile is opened, all the settings you make will be stored in the profile and not applied directly.\nOnly by applying thoose profiles the settings will be applied"))
        self.root.destroy()
    def __init__(self):
        super().__init__(_("Delete Profile"), _("Select one of the profiles which is to delete"))
        self.applybu.config(bg='red')
        self.root.mainloop()

class report(tinyWin):
    def __init__(self, message, title):
        super().__init__(message, title)
        self.OK = tk.Button(self.root, text="OK", command=self.root.destroy)
        self.OK.grid(row=1, column=0)
        self.root.mainloop()
        
class quest(tinyWin):
    def __init__(self, message, title):
        super().__init__(message, title)
        self.text = ""
        self.OK = tk.Button(self.root, text="OK", bg="green", command=self.entry)
        self.root.bind('<Return>', self.entry)
        self.txt = tk.Entry(self.root, width=25)
        self.txt.grid(row=1, column=0)
        self.OK.grid(row=2, column=0)
        
class newFile(quest):
    def entry(self, event=None):
        oldname = live.fname
        live.fname = self.txt.get()
        live.safe(False)
        live.fname = oldname
        self.root.destroy()
    def __init__(self):
        super().__init__(_("Please enter the new Profiles name.\nThe current setting will be overtoken to that.\nAfter safing, the safed profile won't be opened automatically.\nTo open the new Profile, go to profile -> open and select it."), _("New Profile"))
        self.root.mainloop()
        
class chName(quest):
    def entry(self, event=None):
        self.text = self.txt.get()
        live.chName(self.text, 0)
        live.safe(0)
        self.root.destroy()
    def __init__(self, message, title):
        super().__init__(message, title)
        self.txt.insert(-1, live.fName(0))
        self.root.mainloop()


class pwd(quest):
    def entry(self, event=None):
        global pwd
        sudoer = self.txt.get()
        user = os.popen("echo '"+sudoer+"' | sudo -S whoami")
        if user.read().strip() == "root":
            pwd = sudoer
            self.root.destroy()
        else:
            report(_("Password was incorrect.\nPlease try again.", "Wrong Password"))
    def __init__(self, message, title):
        super().__init__(message, title)
        self.txt.config(show="*")
        self.root.mainloop()
        
        
#General init 
live = profile("logid")
live.fromFile()
live.read(0)
#   Right management, coming along the problems that we are working in a protected directory
user = os.popen('whoami')
root = False
#Find Out if this is the first run and if it is, ask if the device name is correct.
if live.now.find('First-run') != -1:
    #setup... in case of aur installation made by PKGBUILD.
    #This is indeed the first run.
    #if user.read().strip() == "root":
        #root = True
    #if not root:
        #pwd("This Programm needs root access to save the files correctly.\nPlease entry the password so that the Programm has the rights needed.","Password required")
    
    #make the profile folder and give up permissions:
    #if root:
        #store = os.popen("mkdir /etc/logiops-profiles && echo '"+pwd+"' | sudo -S chmod 766 /etc/logid.cfg && echo '"+pwd+"' | sudo -S chmod 777 -R /etc/logiops-profiles && systemctl enable logid.service && systemctl start logid")
    #else:
        #store = os.popen("echo '"+pwd+"' | sudo -S mkdir /etc/logiops-profiles && echo '"+pwd+"' | sudo -S chmod 766 /etc/logid.cfg && echo '"+pwd+"' | sudo -S chmod 777 -R /etc/logiops-profiles && echo '"+pwd+"' | sudo -S systemctl enable logid.service && echo '"+pwd+"' | sudo -S systemctl start logid")
    chName(_("Please check if this is the correct device name.\nIf it is, just click OK, if not, please adjust and click OK"), _("Adjust device name"))
    live.now = live.now.replace('First-run', '')
    print(live.now)
    live.safe(False)
    
#Root window stuff
def geUP():
    dialog = simplechoose(_("Upward Gesture"), _("Set upwards Gesture"), "Up",  False)
def geDOWN():
    dialog = simplechoose("Downwards Gesture", "Set downwards Gesture", "Down",  False)
def geLEFT():
    dialog = simplechoose(_("Left Side Gesture"), _("Set left Gesture"), "Left",  False)
def geRIGHT():
    dialog = simplechoose(_("Right Side Gesture"), _("Set right Gesture"), "Right",  False)
def geFWD():
    dialog = simplechoose(_("Forward Button"), _("Set forward button click"), "0xc4",  False)
def geBCK():
    dialog = simplechoose(_("Backward Button"), _("Set backward button click"), "0x53",  False)
def changeName():
    dialog = chName(_("Please enter the new Device Name:"), _("Change Device Name"))
def openProfile():
    dialog = openFile()
def newProfile():
    dialog = newFile()
def applyProfile():
    applyFile()
    
def delProfile():
    delFile()
def appInfo():
    dialog = report(_("LOGIOPS CONFIGURATOR\n\nThis programm is designed to configure the logiops driver (for Logitech MX Mouses) with a graphical interface.\nYou can define gestures\n(so that you for example press the gesture button push the mouse upwards, realse the button and some magic happens (;  )\n, define button actions and save those actions in profiles and apply thoose to the browser settings.\nThe profile opened in standart case is the live profile, which will apply the settings directly into the driver config file\nThe programm doesn't provide all posibilities, if you know enough about linux you can also edit the files directly.\nThe live profile is located at /etc/logid.cfg the other profile files at /usr/share/logiops-configurator\nPlease report errors but don't be angry at me, at the moment I am the only developer and this is a young project."), _("About Logiops Configurator"))
    dialog.root.geometry('600x300')
app = tk.Tk() 
app.geometry('900x600')
app.title("Logiops Configurator")

menubar = tk.Menu(app)

pronu = tk.Menu(menubar)
pronu.add_command(label=_("New"), command=newProfile)
pronu.add_command(label=_("Open"), command=openProfile)
pronu.add_command(label=_("Delete"), command=delFile)
pronu.add_command(label=_("Apply"), command=applyProfile)
menubar.add_cascade(label=_("Profiles"), menu=pronu)

bumenu = tk.Menu(menubar)
bumenu.add_command(label=_("Forward"), command=geFWD)
bumenu.add_command(label=_("Backward"), command=geBCK)

menubar.add_cascade(label=_("Buttons"), menu=bumenu)

gemenu = tk.Menu(menubar)
gemenu.add_command(label=_("Up"), command=geUP)
gemenu.add_command(label=_("Down"), command=geDOWN)
gemenu.add_command(label=_("Left"), command=geLEFT)
gemenu.add_command(label=_("Right"), command=geRIGHT)
menubar.add_cascade(label=_("Gestures"), menu=gemenu)

genmenu = tk.Menu(menubar)
genmenu.add_command(label=_("Change Device Name"), command=changeName)
genmenu.add_command(label=_("About..."), command=appInfo)
genmenu.add_command(label=_("Close programm"), command=app.destroy)

menubar.add_cascade(label=_("General"), menu=genmenu)

advice = tk.Label(app, text=_("Currently, the Live Profile is opened. That means, that all changes will be applied directly into the driver settings.\nWith the help of the Profile menu you may create several profiles with different settings and also for several devices if needed.\nWhen another Profile than the Live Profile is opened, all the settings you make will be stored in the profile and not applied directly.\nOnly by applying thoose profiles the settings will be applied"))
advice.grid(row=0, column=0)



app.config(menu=menubar)

app.mainloop() 
